require('browser-sync').init({
  port: 3000,
  server: {
    baseDir: "src"
  },
  ui: {
    port: 8080,
    weinre: {
      port: 9090
    }
  },
  watch: true
})
