define(['d3', 'flare'], (d3, data) => {
  return {
    template: `
<div class="page">
<h2>D3 Radial Dendrogram</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-radial-dendrogram">observablehq.com</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>`,
    mounted () {
      const M = Math;
      const width = 720;
      const radius = width / 2;
      const tree = d3.cluster().size([2 * M.PI, radius - 100]);

      (() => {
        const root = tree(d3.hierarchy(data))
          .sort((a, b) => (a.height - b.height) || a.data.name.localeCompare(b.data.name));
        
        const svg = d3.select('#drawing')
          .append('svg')
          .style('width', width)
          .style('height', width)
          .style('padding', '10px')
          .style('box-sizing', 'border-box')
          .style('font', '10px sans serif'); 

        const g = svg.append('g');

        const link = g.append('g')
          .attr('fill', 'none')
          .attr('stroke', '#555')
          .attr('stroke-opacity', 0.4)
          .attr('stroke-width', 1.5)
          .selectAll('path')
          .data(root.links())
          .enter().append('path')
          .attr('d', d3.linkRadial()
            .angle(d => d.x)
            .radius(d => d.y));

        const node = g.append('g')
          .attr('stroke-linejoin', 'round')
          .attr('stroke-width', 3)
          .selectAll('g')
          .data(root.descendants().reverse())
          .enter().append('g')
          .attr('transform', d =>
            `rotate(${d.x * 180 / M.PI - 90}) translate(${d.y},0)`);

        node.append('circle')
          .attr('fill', d => d.children ? '#555' : '#999')
          .attr('r', 2.5);

        node.append('text')
          .attr('dy', '.31em')
          .attr('x', d => d.x < M.PI === !d.children ? 6 : -6)
          .attr('text-anchor', d => d.x < M.PI === !d.children ? 'start' : 'end')
          .attr('transform', d => d.x >= M.PI ? 'rotate(180)' : null)
          .text(d => d.data.name)
          .filter(d => d.children)
          .clone(true).lower()
          .attr('stroke', 'white');

        const box = g.node().getBBox();

        svg
          .attr('width', box.width)
          .attr('height', box.height)
        .attr('viewBox', `${box.x} ${box.y} ${box.width} ${box.height}`);
      })();
    },
  };
});
