define(['d3',
  'topo-client',
  'https://unpkg.com/@observablehq/unemployment@0.0.1/index.js',
], (d3, topojson, dataMap) => {
  return {
    template: `
<div class="page">
<h2>D3 Choropleth</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-choropleth">observablehq.com</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>
    `,
    async mounted () {
      const us = await d3.json('https://unpkg.com/us-atlas@1/us/10m.json');
      const format = d3.format('');
      const color = d3.scaleQuantize()
        .domain([1, 10])
        .range(d3.schemeBlues[9]);
      const data = (() => {
        let data = new Map(dataMap.map(d => [d.id, d.rate]));
        data.title = 'Unemployment rate (%)';
        return data;
      })();

      const chart = (() => {
        const width = 960;
        const height = 600;
        const path = d3.geoPath();

        const x = d3.scaleLinear()
          .domain(d3.extent(color.domain()))
          .rangeRound([600, 860]);

        const svg = d3.select('#drawing')
          .append('svg')
          .style('width', width)
          .style('height', height);

        const g = svg.append('g')
          .attr('transform', 'translate(0, 40)');
        
        g.selectAll('rect')
          .data(color.range().map(d => color.invertExtent(d)))
          .enter().append('rect')
          .attr('height', 8)
          .attr('x', d => x(d[0]))
          .attr('width', d => x(d[1]) - x(d[0]))
          .attr('fill', d => color(d[0]));

        g.append('text')
          .attr('class', 'caption')
          .attr('x', x.range()[0])
          .attr('y', -6)
          .attr('fill', '#000')
          .attr('text-anchor', 'start')
          .attr('font-weight', 'bold')
          .text(data.title);

        g.call(d3.axisBottom(x)
          .tickSize(13)
          .tickFormat(format)
          .tickValues(color.range().slice(1).map(d => color.invertExtent(d)[0])))
          .select('.domain')
          .remove();

        svg.append('g')
          .selectAll('path')
          .data(topojson.feature(us, us.objects.counties).features)
          .enter().append('path')
          .attr('fill', d => color(data.get(d.id)))
          .attr('d', path)
          .append('title')
          .text(d => format(data.get(d.id)));

        svg.append('path')
          .datum(topojson.mesh(us, us.objects.states, (a, b) => a !== b))
          .attr('fill', 'none')
          .attr('stroke', 'white')
          .attr('stroke-linejoin', 'round')
          .attr('d', path);

        return svg.node();
      })();
    },
  };
});
