define([
  'd3',
  'flare',
], (d3, data) => {

  return {
    template: `
<div class="page">
<h2>D3 Zoomable Sunburst</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
    <li><a href="https://github.com/d3/d3-hierarchy/blob/master/README.md#hierarchy">hierarchy</a></li>
    <li><a href="https://beta.observablehq.com/@mbostock/d3-zoomable-sunburst">observablehq.com</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>`,
    mounted () {
      const M = Math;
      const width = 720;
      const radius = width / 6;
      const format = d3.format(',d');
      const color = d3.scaleOrdinal().range(d3.quantize(d3.interpolateRainbow, data.children.length + 1));

      const arc = d3.arc()
        .startAngle(d => d.x0)
        .endAngle(d => d.x1)
        .padAngle(d => M.min((d.x1 - d.x0) / 2, .005))
        .padRadius(radius * 1.5)
        .innerRadius(d => d.y0 * radius)
        .outerRadius(d => M.max(d.y0 * radius, d.y1 * radius - 1));

      const partition = data => {
        const root = d3.hierarchy(data)
          .sum(d => d.size)
          .sort((a, b) => b.value - a.value);
        return d3.partition()
          .size([2 * M.PI, root.height + 1])
        (root);
      };

      const chart = (() => {
        const root = partition(data);

        root.each(d => d.current = d);

        const arcVisible = (d) => {
          return d.y1 <= 3 && d.y0 >= 1 && d.x1 > d.x0;
        };
        const labelVisible = (d) => {
          return d.y1 <= 3 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
        };
        const labelTransform = (d) => {
          const x = (d.x0 + d.x1) / 2 * 180 / M.PI;
          const y = (d.y0 + d.y1) / 2 * radius;
          return `rotate(${x - 90}) translate(${y}, 0) rotate(${x < 180 ? 0 : 180})`; 
        };

        const svg = d3.select('#drawing')
          .style('width', width)
          .style('height', 'auto')
          .append('svg')
          .style('width', width)
          .style('height', width)
          .style('font', '10px sans-serif');
        const g = svg.append('g')
          .attr('transform', `translate(${width / 2}, ${width / 2})`);
        const path = g.append('g')
          .selectAll('path')
          .data(root.descendants().slice(1))
          .enter().append('path')
          .attr('fill', d => { while (d.depth > 1) d = d.parent; return color(d.data.name); })
          .attr('fill-opacity', d => arcVisible(d.current) ? (d.children ? 0.6 : 0.4) : 0)
          .attr('d', d => arc(d.current));

        const label = g.append('g')
          .attr('pointer-events', 'none')
          .attr('text-anchor', 'middle')
          .style('user-select', 'none')
          .selectAll('text')
          .data(root.descendants().slice(1))
          .enter().append('text')
          .attr('dy', '0.35em')
          .attr('fill-opacity', d => +labelVisible(d.current))
          .attr('transform', d => labelTransform(d.current))
          .text(d => d.data.name);

        let parent;

        const clicked = (p) => {
          parent.datum(p.parent || root);

          root.each(d => d.target = {
            x0: M.max(0, M.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) * 2 * M.PI,
            x1: M.max(0, M.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) * 2 * M.PI,
            y0: M.max(0, d.y0 - p.depth),
            y1: M.max(0, d.y1 - p.depth)
          });

          const t = g.transition().duration(750);

          path.transition(t)
            .tween('data', d => {
              const i = d3.interpolate(d.current, d.target);
              return t => d.current = i(t);
            })
            .filter(function (d) {
              return +this.getAttribute('fill-opacity') || arcVisible(d.target);
            })
            .attr('fill-opacity', d => arcVisible(d.target) ? (d.children ? 0.6 : 0.4) : 0)
            .attrTween('d', d => () => arc(d.current));

          label.filter(function (d) {
            return +this.getAttribute('fill-opacity') || labelVisible(d.target);
          }).transition(t)
            .attr('fill-opacity', d => +labelVisible(d.target))
            .attrTween('transform', d => () => labelTransform(d.current));
        };

        parent = g.append('circle')
          .datum(root)
          .attr('r', radius)
          .attr('fill', 'none')
          .attr('pointer-events', 'all')
          .on('click', clicked);

        path.filter(d => d.children)
          .style('cursor', 'pointer')
          .on('click', clicked);

        path.append('title')
          .text(d => `${d.ancestors().map(d => d.data.name).reverse().join('/')}\n${format(d.value)}`);
      })();
    },
  };
});
