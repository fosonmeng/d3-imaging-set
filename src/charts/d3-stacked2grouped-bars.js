define(['d3'], (d3) => {
  return {
    template: `
<div class="page">
<h2>D3 Stacked-to-Grouped Bars</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-stacked-to-grouped-bars">observablehq.com</a></li>
  <li>bumps ??</li>
  </ul>
</nav>
<div id="drawing">
<label for="">stacked<input type="radio" value="stacked" v-model="layout"></label>
<label for="">grouped<input type="radio" value="grouped" v-model="layout"></label>
<br>
</div>
</div>
`,
    data () {
      return {
        layout: 'stacked',
        chart: null,
      };
    },
    watch: {
      layout (val) {
        this.updateChart(val);
      },
    },
    methods: {
      updateChart (layout) {
        this.chart.update(layout);
      },
    },
    mounted () {
      const M = Math;
      const bumps = (m) => {
        const values = [];

        for (let i = 0; i < m; ++i) {
          values[i] = .1 + .1 * M.random();
        }

        for (let j = 0; j < 5; ++j) {
          const x = 1 / (.1 + M.random());
          const y = 2 * M.random() - .5;
          const z = 10 / (.1 + M.random());
          for (let i = 0; i < m; i++) {
            const w = (i / m - y) * z;
            values[i] += x * M.exp(-w * w);
          }
        }

        for (let i = 0; i < m; ++i) {
          values[i] = M.max(0, values[i]);
        }

        return values;
      };

      const margin = {top: 0, right: 0, bottom: 10, left: 0};
      const height = 500;
      const width = 720;
      const m = 58;
      const n = 5;
      const xAxis = svg => svg.append('g')
      .attr('transform', `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x).tickSizeOuter(0).tickFormat(() => ''));

      const yz = d3.range(n).map(() => bumps(m));
      const xz = d3.range(m);

      const z = d3.scaleSequential(d3.interpolateBlues)
      .domain([-.5 * n, 1.5 * n]);

      const y01z = d3.stack()
      .keys(d3.range(n))
      (d3.transpose(yz))
      .map((data, i) => data.map(([y0, y1]) => [y0, y1, i]));

      const y1Max = d3.max(y01z, y => d3.max(y, d => d[1]));
      const yMax = d3.max(yz, y => d3.max(y));

      const y = d3.scaleLinear()
      .domain([0, y1Max])
      .range([height - margin.bottom, margin.top]);

      const x = d3.scaleBand()
      .domain(xz)
      .rangeRound([margin.left, width - margin.right])
      .padding(.08);

      const chart = (() => {
        const svg = d3.select('#drawing')
        .append('svg')
        .style('width', width)
        .style('height', height);

        const rect = svg.selectAll('g')
        .data(y01z)
        .enter().append('g')
        .attr('fill', (d, i) => z(i))
        .selectAll('rect')
        .data(d => d)
        .enter().append('rect')
        .attr('x', (d, i) => x(i))
        .attr('y', height - margin.bottom)
        .attr('width', x.bandwidth())
        .attr('height', 0);

        svg.append('g')
        .call(xAxis);

        const transitionGrouped = () => {
          y.domain([0, yMax]);
          rect.transition()
          .duration(500)
          .delay((d, i) => i * 20)
          .attr('x', (d, i) => x(i) + x.bandwidth() / n * d[2])
          .attr('width', x.bandwidth() / n)
          .transition()
          .attr('y', d => y(d[1] - d[0]))
          .attr('height', d => y(0) - y(d[1] - d[0]));
        };
        
        const transitionStacked = () => {
          y.domain([0, y1Max]);
          rect.transition()
          .duration(500)
          .delay((d, i) => i * 20)
          .attr('y', d => y(d[1]))
          .attr('height', d => y(d[0]) - y(d[1]))
          .transition()
          .attr('x', (d, i) => x(i))
          .attr('width', x.bandwidth());
        };

        const update = (layout) => {
          if (layout === 'stacked') transitionStacked();
          else transitionGrouped();
        };

        return Object.assign(svg.node(), {update});
      })();

      this.chart = chart;
      this.updateChart(this.layout);
    },
  };
});
