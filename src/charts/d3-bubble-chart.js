define(['d3', 'flare', 'uuid'], (d3, flare, uuid) => {
  return {
    template: `
<div class="page">
<h2>D3 Bubble Chart</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-bubble-chart">observablehq.com</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>`,
    mounted () {
      const color = d3.scaleOrdinal().range(d3.schemeCategory10);
      const format = d3.format(',d');
      const height = 932;
      const width = 932;
      const pack = (data) => d3.pack()
        .size([width - 2, height - 2])
        .padding(3)(d3.hierarchy({
            children: data  
          })
          .sum((d) => d.value));
      const data = d3.hierarchy(flare)
        .leaves()
        .map((d) => {
          let p = d;
          while (p.depth > 1) p = p.parent;
          d.data.title = d.ancestors().reverse().map((a) => a.data.name).join('/');
          d.data.group = p.data.name;
          return d;
        })
        .map((d) => d.data)
        .sort((a, b) => a.group.localeCompare(b.group))
        .map(({name, title, group, size}) => ({name, title, group, value: size}));

      const chart = (() => {
        const root = pack(data);
        const svg = d3.select('#drawing')
          .append('svg')
          .style('width', width)
          .style('height', height)
          .attr('font-size', 10)
          .attr('font-family', 'sans-serif')
          .attr('text-anchor', 'middle');
        const leaf = svg.selectAll('g')
          .data(root.leaves())
          .enter().append('g')
            .attr('transform', (d) => `translate(${d.x + 1},${d.y + 1})`);
        leaf.append('circle')
          .attr('id', (d) => (d.leafUid = uuid()))
          .attr('r', (d) => d.r)
          .attr('fill-opacity', .7)
          .attr('fill', (d) => color(d.data.group));
        leaf.append('clipPath')
            .attr('id', (d) => (d.clipUid = uuid()))
          .append('use')
            .attr('xlink:href', (d) => d.leafUid.href);
        leaf.append('text')
            .attr('clip-path', (d) => d.clipUid)
          .selectAll('tspan')
          .data((d) => d.data.name.split(/(?=[A-Z][^A-Z])/g))
          .enter().append('tspan')
            .attr('x', 0)
            .attr('y', (d, i, nodes) => `${i - nodes.length / 2 + .8}em`)
            .text((d) => d);
        leaf.append('title')
          .text((d) => `${d.data.title}\n${format(d.value)}`);

        return svg.node();
      })();
    },
  }
});
