define([
  'd3',
  'd3-sankey',
], (d3, Sankey) => {
  Object.assign(d3, Sankey);

  return {
    template: `
<div class="page">
<h2>D3 Sankey Diagram</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  </ul>
</nav>
<div id="drawing"></div>
</div>`,
    async mounted () {
      const M = Math;
      const width = 720;
      const height = 420;
      const data = await d3.json("https://gist.githubusercontent.com/mbostock/ca9a0bb7ba204d12974bca90acc507c0/raw/398136b7db83d7d7fd89181b080924eb76041692/energy.json");
      const color = (() => {
        const color = d3.scaleOrdinal(d3.schemeCategory10);
        return name => color(name.replace(/ .*/, ''));
      })();
      const format = (() => {
        const f = d3.format(',.0f');
        return d => `${f(d)} TWh`;
      })();
      let edgeColor = '';

      const sankey = (() => {
        const sankey = d3.sankey()
          .nodeWidth(15)
          .nodePadding(10)
          .extent([[1, 1], [width - 1, height - 5]]);
        return ({nodes, links}) => sankey({
          nodes: nodes.map(d => Object.assign({}, d)),
          links: links.map(d => Object.assign({}, d)),
        });
      })();

      const chart = (() => {
        const svg = d3.select('#drawing')
          .append('svg')
          .style('width', width)
          .style('height', height);

        const {nodes, links} = sankey(data);

        svg.append('g')
          .attr('stroke', '#000')
          .selectAll('rect')
          .data(nodes)
          .enter().append('rect')
          .attr('x', d => d.x0)
          .attr('y', d => d.y0)
          .attr('height', d => d.y1 - d.y0)
          .attr('width', d => d.x1 - d.x0)
          .attr('fill', d => color(d.name))
          .append('title')
          .text(d => `${d.name}\n${format(d.value)}`);

        const link = svg.append('g')
          .attr('fill', 'none')
          .attr('stroke-opacity', 0.5)
          .selectAll('g')
          .data(links)
          .enter().append('g')
          .style('mix-blend-mode', 'multiply');

        if (edgeColor === 'path') {
          const gradient =link.append('linearGradient')
            .attr('id', d => (d.uid = `link${uid()}`))
            .attr('gradientUnits', 'userSpaceOnUse')
            .attr('x1', d => d.source.x1)
            .attr('x2', d => d.target.x0);

          gradient.append('stop')
            .attr('offset', '0%')
            .attr('stop-color', d => color(d.source.name));

          gradient.append('stop')
            .attr('offset', '100%')
            .attr('stop-color', d => color(d.target.name));
        }

        link.append('path')
          .attr('d', d3.sankeyLinkHorizontal())
          .attr('stroke', d => edgeColor === 'path' ? d.uid
            : edgeColor === 'input' ? color(d.source.name)
            : color(d.target.name))
          .attr('stroke-width', d => M.max(1, d.width));

        link.append('title')
          .text(d => `${d.source.name} -> ${d.target.name}\n${format(d.value)}`);

        svg.append('g')
          .style('font', '10px sans-serif')
          .selectAll('text')
          .data(nodes)
          .enter().append('text')
          .attr('x', d => d.x0 < width / 2 ? d.x1 + 6 : d.x0 - 6)
          .attr('y', d => (d.y1 + d.y0) / 2)
          .attr('dy', '.35em')
          .attr('text-anchor', d => d.x0 < width / 2 ? 'start' : 'end')
          .text(d => d.name);
      })();
    },
  };
});
