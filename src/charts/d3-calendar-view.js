define(['d3', 'dji'], (d3, dji) => {
  return {
    template: `
<div class="page">
<h2>D3 Calendar View</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-calendar-view">observablehq.com</a></li>
  <li><a href="https://github.com/d3/d3-time/blob/master/README.md#timeYear">#timeYear</a></li>
  <li><a href="https://github.com/d3/d3-collection/blob/master/README.md#nest">#d3.nest</a></li>
  </ul>
</nav>
<select id="" name="" v-model="weekday">
  <option value="weekday">Weekdays only</option>
  <option value="sunday">Sunday-based weeks</option>
  <option value="monday">Monday-based weeks</option>
</select>
<div id="drawing"></div>
</div>`,
    data () {
      return {
        weekday: 'weekday'
      };
    },
    watch: {
      weekday (val) {
        this.redraw();
      },
    },
    mounted () {
      this.redraw();
    },
    methods: {
      redraw () {
        d3.select('#drawing').selectAll('svg').remove();

        const weekday = this.weekday;
        const M = Math;
        const width = 964;
        const cellSize = 17;
        const height = cellSize * (weekday === 'weekday' ? 7 : 9);
        const data = d3.pairs(dji, ({close: previous}, {date, close}) => {
          return {date, value: (close - previous) / previous};
        });
        const color = d3.scaleSequential(d3.interpolatePiYG).domain([-.05, .05]);
        const formatMonth = d3.timeFormat('%b');
        const formatDay = (d) => "SMTWTFS"[d.getDay()];
        const formatDate = d3.timeFormat('%x');
        const format = d3.format('+.2%');
        const timeWeek = weekday === 'sunday' ? d3.timeSunday : d3.timeMonday;
        const countDay = weekday === 'sunday' ? (d) => d.getDay() : (d) => (d.getDay() + 6) % 7;
        const pathMonth = (t) => {
          const n = weekday === 'weekday' ? 5 : 7;
          const d = M.max(0, M.min(n, countDay(t)));
          const w = timeWeek.count(d3.timeYear(t), t);
          return `${d === 0 ? `M${w * cellSize},0`
            : d === n ? `M${(w + 1) * cellSize},0`
            : `M${(w + 1) * cellSize},0V${d * cellSize}H${w * cellSize}`}V${n * cellSize}`;
        };

        const chart = (() => {
          const years = d3.nest()
              .key((d) => d.date.getFullYear())
            .entries(data)
            .reverse();

          const svg = d3.select('#drawing').append('svg')
            .style('width', width)
            .style('height', height)
            .style('font', '10px sans-serif');

          const year = svg.selectAll('g')
            .data(years)
            .enter().append('g')
              .attr('transform', (d, i) => {
                svg.style('height', height * (i + 1) + cellSize * 1.5);
                return `translate(40,${height * i + cellSize * 1.5})`;
              });

          year.append('text')
            .attr('x', -5)
            .attr('y', -5)
            .attr('font-weight', 'bold')
            .attr('text-anchor', 'end')
            .text((d) => d.key);

          year.append('g')
              .attr('text-anchor', 'end')
            .selectAll('text')
            .data((weekday === 'weekday' ? d3.range(2, 7) : d3.range(7)).map((i) => new Date(1995, 0, i)))
            .enter().append('text')
              .attr('x', -5)
              .attr('y', (d) => {
                return (countDay(d) + .5) * cellSize
              })
              .attr('dy', '.31em')
              .text(formatDay);

          let maxX = 0;
          let maxY = 0;

          year.append('g')
            .selectAll('rect')
            .data((d) => d.values)
            .enter().append('rect')
              .attr('width', cellSize - 1)
              .attr('height', cellSize - 1)
              .attr('x', (d) => {
                const x = timeWeek.count(d3.timeYear(d.date), d.date) * cellSize + .5;
                if (x > maxX) maxX = x;
                return x;
              })
              .attr('y', (d) => countDay(d.date) * cellSize + .5)
              .attr('fill', (d) => color(d.value))
            .append('title')
              .text((d) => `${formatDate(d.date)}: ${format(d.value)}`);

          const month = year.append('g')
            .selectAll('g')
            .data((d) => d3.timeMonths(d3.timeMonth(d.values[0].date), d.values[d.values.length - 1].date))
            .enter().append('g');

          month.filter((d, i) => i).append('path')
            .attr('fill', 'none')
            .attr('stroke', '#fff')
            .attr('storke-width', 3)
            .attr('d', pathMonth);

          month.append('text')
            .attr('x', (d) => timeWeek.count(d3.timeYear(d), timeWeek.ceil(d)) * cellSize + 2)
            .attr('y', -5)
            .text(formatMonth);

          return svg.node();
        })();
      },
    },
  };
});
