define(['d3'], (d3) => {
  return {
    template: `
<div class="page">
<h2>D3 Box Plot</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-box-plot">observablehq.com</a></li>
  <li><a href="https://github.com/d3/d3-array/blob/master/README.md#histograms">#histograms</a></li>
  <li><a href="https://github.com/d3/d3-scale/blob/master/README.md#_quantile">#quantile</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>`,
    async mounted () {
      const M = Math;
      const margin = {top: 20, right: 20, bottom: 30, left: 40 };
      const height = 600;
      const width = 720;
      const n = width / 40;
      const yAxis = (g) => {
        g.attr('transform', `translate(${margin.left},0)`)
          .call(d3.axisLeft(y).ticks(null, 's'))
          .call((g) => g.select('.domain').remove());
      };
      const xAxis = (g) => {
        g.attr('transform', `translate(0,${height - margin.bottom})`)
          .call(d3.axisBottom(x).ticks(n).tickSizeOuter(0));
      };

      const data = await d3.csv("https://gist.githubusercontent.com/mbostock/d63e6019c63887e39e44646696abfb69/raw/5b2b15b4c652167f6c038e717bbe3385dbb9bb99/diamonds.csv",
          ({carat, price}) => ({x: +carat, y: +price}));
      const bins = d3.histogram()
        .thresholds(n)
        .value((d) => d.x)(data)
          .map((bin) => {
            bin.sort((a,b) => a.y - b.y);
            const values = bin.map((d) => d.y);
            const min = values[0];
            const max = values[values.length - 1];
            const q1 = d3.quantile(values, .25);
            const q2 = d3.quantile(values, .5);
            const q3 = d3.quantile(values, .75);
            const iqr = q3 - q1;
            const r0 = M.max(min, q1 - iqr * 1.5);
            const r1 = M.min(max, q3 + iqr * 1.5);
            bin.quartiles = [q1, q2, q3];
            bin.range = [r0, r1];
            bin.outliers = bin.filter((v) => v.y < r0 || v.y > r1);
            return bin;
          })
          .filter((bin) => bin.length) /*will raise error if length is 0*/;

      const x = d3.scaleLinear()
        .domain([d3.min(bins, (d) => d.x0), d3.max(bins, (d) => d.x1)])
        .rangeRound([margin.left, width - margin.right]);
      const y = d3.scaleLinear()
        .domain([d3.min(bins, (d) => d.range[0]), d3.max(bins, (d) => d.range[1])]).nice()
        .range([height - margin.bottom, margin.top]);

      const chart = (() => {
        const svg = d3.select('#drawing')
          .append('svg')
          .style('width', width)
          .style('height', height);
        const g = svg.append('g')
          .selectAll('g')
          .data(bins)
          .enter().append('g');

        g.append('path')
          .attr('stroke', 'currentColor')
          .attr('d', (d) => `M${x((d.x0 + d.x1) / 2)},${y(d.range[1])}V${y(d.range[0])}`);

        g.append('path')
          .attr('fill', '#ddd')
          .attr('d', (d) => `M${x(d.x0) + 1},${y(d.quartiles[2])}H${x(d.x1)}V${y(d.quartiles[0])}H${x(d.x0) + 1}Z`);

        g.append('path')
          .attr('stroke', 'currentColor')
          .attr('storke-width', 2)
          .attr('d', (d) => `M${x(d.x0) + 1},${y(d.quartiles[1])}H${x(d.x1)}`);

        g.append('g')
            .attr('fill', 'currentColor')
            .attr('fill-opacity', .2)
            .attr('stroke', 'none')
            .attr('transform', (d) => `translate(${x((d.x0 + d.x1) / 2)},0)`)
          .selectAll('circle')
          .data((d) => d.outliers)
          .enter().append('circle')
            .attr('r', 2)
            .attr('cx', () => (M.random() - .5) * 4)
            .attr('cy', (d) => y(d.y));

        svg.append('g')
          .call(xAxis);
        svg.append('g')
          .call(yAxis);

        return svg.node();
      })();
    },
  };
});
