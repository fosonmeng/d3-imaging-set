define(['d3'], (d3) => {
  return {
    template: `
<div class="page">
<h2>D3 Force-Directed Graph</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/d3-force-directed-graph">observablehq.com</a></li>
  <li>invalidation ??</li>
  </ul>
</nav>
<div id="drawing"></div>
</div>
`,
    async mounted () {
      const drag = simulation => {
        function dragstarted(d) {
          if (!d3.event.active) simulation.alphaTarget(0.3).restart();
          d.fx = d.x;
          d.fy = d.y;
        }

        function dragged(d) {
          d.fx = d3.event.x;
          d.fy = d3.event.y;
        }

        function dragended(d) {
          if (!d3.event.active) simulation.alphaTarget(0);
          d.fx = null;
          d.fy = null;
        }

        return d3.drag()
          .on('start', dragstarted)
          .on('drag', dragged)
          .on('end', dragended);
      };

      const color = (() => {
        const scale = d3.scaleOrdinal(d3.schemeCategory10);
        return d => scale(d.group);
      })();

      const height = 600;
      const width = height;

      const data = await d3.json("https://gist.githubusercontent.com/mbostock/4062045/raw/5916d145c8c048a6e3086915a6be464467391c62/miserables.json");

      const chart = (() => {
        const links = data.links.map(d => Object.create(d));
        const nodes = data.nodes.map(d => Object.create(d));

        const simulation = d3.forceSimulation(nodes)
        .force('link', d3.forceLink(links).id(d => d.id))
        .force('charge', d3.forceManyBody())
        .force('center', d3.forceCenter(width / 2, height / 2));

        const svg = d3.select('#drawing')
        .append('svg')
        .style('width', width)
        .style('height', height);

        const link = svg.append('g')
        .attr('stroke', '#999')
        .attr('stroke-opacity', .6)
        .selectAll('line')
        .data(links)
        .enter().append('line')
        .attr('stroke-width', d => Math.sqrt(d.value));

        const node = svg.append('g')
        .attr('stroke', '#fff')
        .attr('stroke-width', 1.5)
        .selectAll('circle')
        .data(nodes)
        .enter().append('circle')
        .attr('r', 5)
        .attr('fill', color)
        .call(drag(simulation));

        node.append('title')
        .text(d => d.id);

        simulation.on('tick', () => {
        link.attr('x1', d => d.source.x)
        .attr('y1', d => d.source.y)
        .attr('x2', d => d.target.x)
        .attr('y2', d => d.target.y);

        node.attr('cx', d => d.x)
        .attr('cy', d => d.y);
        });

      invalidation.then(() => simulation.stop());

      return svg.node();
      })();
    },
  };
});
