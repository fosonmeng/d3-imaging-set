define(['d3'
], (d3) => {
  return {
    template: `
<div class="page">
<h2>Epicyclic Cearing</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/epicyclic-gearing">observablehq.com</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>
`,
    created () {
      this.animationFrame = null;
    },
    mounted () {
      const gear = ({teeth, radius, annulus}) => {
        const n = teeth;
        let r2 = M.abs(radius);
        let r0 = r2 - toothRadius;
        let r1 = r2 + toothRadius;
        let r3 = holeRadius;
        if (annulus) {
          r3 = r0,
            r0 = r1,
            r1 = r3,
            r3 = r2 + toothRadius * 3;
        }
        const da = M.PI / n;
        let a0 = -M.PI / 2 + (annulus ? M.PI / n : 0);
        let path = ['M', r0 * M.cos(a0), ',', r0 * M.sin(a0)];
        let i = -1;
        while (++i < n) {
          path.push(
            'A', r0, ',', r0, ' 0 0,1 ', r0 * M.cos(a0 += da), ',', r0 * M.sin(a0),
            'L', r2 * M.cos(a0), ',', r2 * M.sin(a0),
            'L', r1 * M.cos(a0 += da / 3), ',', r1 * M.sin(a0),
            'A', r1, ',', r1, ' 0 0,1 ', r1 * M.cos(a0 += da / 3), ',', r1 * M.sin(a0),
            'L', r2 * M.cos(a0 += da / 3), ',', r2 * M.sin(a0),
            'L', r0 * M.cos(a0), ',', r0 * M.sin(a0)
          );
        }
        path.push('M0,', -r3, 'A', r3, ',', r3, ' 0 0,0 0,', r3, 'A', r3, ',', r3, ' 0 0,0 0,', -r3, 'Z');
        return path.join('');
      };

      const M = Math;
      const width = 720;
      const frameRadius = 360;
      const speed = 2;
      const holeRadius = 10;
      const toothRadius = 4;
      const radius = 45;

      const anim = function* () {
        const height = 500;
        const x = M.sin(2 * M.PI / 3);
        const y = M.cos(2 * M.PI / 3);
        const start = Date.now();

        const svg = d3.select('#drawing')
          .append('svg')
          .style('width', width)
          .style('height', height);

        const frame = svg.append('g')
          .attr('transform', `translate(${width / 2},${height / 2})`)
          .append('g')
          .datum({radius: +frameRadius});

        const path = frame.selectAll('g')
          .data([
            {fill: '#c6dbef', teeth: 80, radius: -radius * 5, origin: [0,0], annulus: true},
            {fill: '#6baed6', teeth: 16, radius: radius, origin: [0,0]},
            {fill: '#9ecae1', teeth: 32, radius: -radius * 2, origin: [0,-radius * 3]},
            {fill: '#9ecae1', teeth: 32, radius: -radius * 2, origin: [-radius * 3 * x,-radius * 3 * y]},
            {fill: '#9ecae1', teeth: 32, radius: -radius * 2, origin: [radius * 3 * x,-radius * 3 * y]}
          ])
          .enter().append('g')
          .attr('transform', d => `translate(${d.origin})`)
          .append('path')
          .attr('stroke', 'black')
          .attr('fill', d => d.fill)
          .attr('d', gear);

        while (true) {
          const angle = (Date.now() - start) * speed;
          const transform = d => `rotate(${angle / d.radius})`;
          path.attr('transform', transform);
          frame.attr('transform', transform);
          yield svg.node();
        }
      };

      const animating = anim();
      const that = this;
      that.animationFrame = requestAnimationFrame(function frame () {
        if (animating.next()) {
          that.animationFrame = requestAnimationFrame(frame);
        }
      });
    },
    beforeDestroy () {
      cancelAnimationFrame(this.animationFrame);
    }
  };
});
