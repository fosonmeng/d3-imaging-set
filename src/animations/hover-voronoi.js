define(['d3', 'd3-delaunay'], (d3, d3Delaunay) => {
  Object.assign(d3, d3Delaunay);

  return {
    template: `
<div class="page">
<h2>Hover Voronoi</h2>
<nav id="nav-api">
  <h3>Navigation Api References</h3>
  <ul>
  <li><a href="https://beta.observablehq.com/@mbostock/hover-voronoi">observablehq.com</a></li>
  </ul>
</nav>
<div id="drawing"></div>
</div>
    `,
    mounted () {
      const width = 720;
      const height = 600;
      const n = 200;
      const M = Math;

      const canvas = (() => {
        const context = d3.select('#drawing')
        .append('canvas')
        .property('width', width)
        .property('height', height)
        .node().getContext('2d');
        const particles = Array.from({length:n}, () => [
          M.random() * width, M.random() * height 
        ]);
        
        const update = () => {
          const delaunay = d3.Delaunay.from(particles);
          const voronoi = delaunay.voronoi([.5, .5, width - .5, height - .5]);
          context.clearRect(0, 0, width, height);

          context.beginPath();
          delaunay.render(context);
          context.strokeStyle = '#ccc';
          context.stroke();

          context.beginPath();
          voronoi.render(context);
          context.strokeStyle = '#000';
          context.stroke();

          context.beginPath();
          delaunay.renderPoints(context);
          context.fill();
        };

        context.canvas.outouchmove = context.canvas.onmousemove = (event) => {
          event.preventDefault();
          particles[0] = [event.layerX, event.layerY];
          update();
        };

        update();

        return context.canvas;
      })();
    },
  };
});
